/**
 * 
 */
package caixa_blanca;

/**@author Álvaro Owen de la Quintana
 * JUnitProves 
 * 24 feb 2023
 */
public class DiaSetmana {

	public static String calcularDiaSetmana(int dia, int mes, int any) {
        if (mes < 1 || mes > 12 || dia < 1 || dia > 31) { //1, 2, 3, 4
            return "Data incorrecta"; //5
        }
        
        if (mes == 2) { //6
            if (dia > 29 || (dia == 29 && !esAnyTraspàs(any))) { //7, 8, 9
                return "Data incorrecta"; //10
            }
        } else if (mes == 4 || mes == 6 || mes == 9 || mes == 11) { //11, 12, 13, 14
            if (dia > 30) { //15
                return "Data incorrecta"; //16
            }
        }
        
        int a = (14 - mes) / 12;
        int y = any - a;
        int m = mes + 12 * a - 2;
        int d = (dia + y + y/4 - y/100 + y/400 + (31*m)/12) % 7;
        
        switch (d) { //17
            case 0:
                return "Diumenge"; //18
            case 1:
                return "Dilluns"; //19
            case 2:
                return "Dimarts"; //20
            case 3:
                return "Dimecres"; //21
            case 4:
                return "Dijous"; //22
            case 5:
                return "Divendres"; //23
            case 6:
                return "Dissabte"; //24
            default:
                return "Error en el càlcul"; //25
        }
    }
    
    public static boolean esAnyTraspàs(int any) {
        return (any % 4 == 0 && any % 100 != 0) || (any % 400 == 0);
    }
}
