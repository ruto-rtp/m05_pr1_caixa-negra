package caixa_blanca;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class DiaSetmanaTest {

	@Test
	void test() {
/*1*/  	assertEquals("Data incorrecta", DiaSetmana.calcularDiaSetmana(1, 0, 10)); //dia, mes, any
/*2*/  	assertEquals("Data incorrecta", DiaSetmana.calcularDiaSetmana(1, 13, 10)); //dia, mes, any
/*3*/  	assertEquals("Data incorrecta", DiaSetmana.calcularDiaSetmana(0, 1, 10)); //dia, mes, any
/*4*/	assertEquals("Data incorrecta", DiaSetmana.calcularDiaSetmana(32, 1, 10)); //dia, mes, any
    	
/*5*/	assertEquals("Data incorrecta", DiaSetmana.calcularDiaSetmana(30, 2, 10)); //dia, mes, any
/*6*/	assertEquals("Data incorrecta", DiaSetmana.calcularDiaSetmana(29, 2, 100)); //dia, mes, any
/*7*/	assertEquals("Dissabte", DiaSetmana.calcularDiaSetmana(29, 2, 1992)); //dia, mes, any

/*8*/	assertEquals("Data incorrecta", DiaSetmana.calcularDiaSetmana(31, 4, 10)); //dia, mes, any
/*9*/	assertEquals("Data incorrecta", DiaSetmana.calcularDiaSetmana(31, 6, 10)); //dia, mes, any
/*10*/	assertEquals("Data incorrecta", DiaSetmana.calcularDiaSetmana(31, 9, 10)); //dia, mes, any
/*11*/	assertEquals("Data incorrecta", DiaSetmana.calcularDiaSetmana(31, 11, 10)); //dia, mes, any
    	
/*12*/	assertEquals("Diumenge", DiaSetmana.calcularDiaSetmana(24, 2, 8)); //dia, mes, any
/*13*/	assertEquals("Dilluns", DiaSetmana.calcularDiaSetmana(26, 4, 10)); //dia, mes, any
/*14*/	assertEquals("Dimarts", DiaSetmana.calcularDiaSetmana(27, 4, 10)); //dia, mes, any
/*15*/	assertEquals("Dimecres", DiaSetmana.calcularDiaSetmana(28, 4, 10)); //dia, mes, any
/*16*/	assertEquals("Dijous", DiaSetmana.calcularDiaSetmana(29, 4, 10)); //dia, mes, any
/*17*/	assertEquals("Divendres", DiaSetmana.calcularDiaSetmana(30, 4, 10)); //dia, mes, any
/*18*/	assertEquals("Dissabte", DiaSetmana.calcularDiaSetmana(1, 5, 10)); //dia, mes, any
	}

}
