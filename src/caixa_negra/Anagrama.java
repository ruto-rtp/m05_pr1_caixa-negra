package caixa_negra;

public class Anagrama {

	public boolean anagrama(String p1, String p2) {
		
		boolean res;
		
		p1 = p1.toLowerCase();
		p2 = p2.toLowerCase();
		StringBuilder sb = new StringBuilder(p2);
		int anagrama = 0;
		
		for (int i = 0; i < p1.length(); i++) {
			for (int j = 0; j < sb.length(); j++) {
				if (p1.charAt(i) == sb.charAt(j)) {
					anagrama ++;
					sb.deleteCharAt(j);
					break;
				}
			}
		}
		
		if (anagrama == p1.length()) {
			if (!p1.equals(p2)) {
				System.out.println("True"); 
				res = true;
			}else {
				System.out.println("False");
				res = false;
			}
		}else {
			System.out.println("False");
			res = false;
		}
		return res;
	}
	
}
