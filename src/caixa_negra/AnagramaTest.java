package caixa_negra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AnagramaTest {


    @Test
    void test1() {
    	Anagrama a = new Anagrama();
    	assertEquals(false, a.anagrama("Casa", "cama"));
    	assertEquals(false, a.anagrama("Vacaciones", "trabajo"));
    	assertEquals(true, a.anagrama("Hola", "lHoa"));
    	assertEquals(true, a.anagrama("Cine", "cein"));
    	assertEquals(false, a.anagrama("Computadora", "raton"));
    	assertEquals(true, a.anagrama("Dormitorio", "TimroDorio"));
    	assertEquals(false, a.anagrama("Escuela", "alumno"));
    	assertEquals(true, a.anagrama("Algoritmo", "Ortilogma"));
    	assertEquals(false, a.anagrama("Música", "arte"));
    	assertEquals(true, a.anagrama("Amor", "roma"));
    	assertEquals(true, a.anagrama("Tom Marvolo Riddle", "I Am Lord Voldemort"));
    }
}
