package caixa_negra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class HarshadTest {

	@Test
	void test() {
		Harshad h = new Harshad();
		assertEquals(true, h.harshad(18));
		assertEquals(false, h.harshad(94));
		assertEquals(true, h.harshad(1729));
		assertEquals(true, h.harshad(8208));
		assertEquals(false, h.harshad(103));
		assertEquals(false, h.harshad(139));
		assertEquals(false, h.harshad(154));
		assertEquals(true, h.harshad(192));
		assertEquals(false, h.harshad(227));
		assertEquals(true, h.harshad(342));
		assertEquals(true, h.harshad(120));
		assertEquals(false, h.harshad(487));
		assertEquals(false, h.harshad(539));
		assertEquals(true, h.harshad(1729));
		assertEquals(false, h.harshad(1001));
		assertEquals(false, h.harshad(2021));
		assertEquals(true, h.harshad(120));
	}

}
