/**
 * 
 */
package llibreries_i_utilities;

/**
 * @author Pol Ruiz
 *
 * 1r Daw
 * 03-04-2023
 */
public enum EnumCardinalitat {
	NORD, NORD_EST, NORD_OEST, SUD, SUD_EST, SUD_OEST, EST, OEST
}
