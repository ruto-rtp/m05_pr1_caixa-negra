/**
 * 
 */
package llibreries_i_utilities;

/**
 * @author Pol Ruiz
 *
 * 1r Daw
 * 03-04-2023
 */
public enum EnumEstatsFeina {
	PENDENT, EN_PROGRES, COMPLETADA
}
