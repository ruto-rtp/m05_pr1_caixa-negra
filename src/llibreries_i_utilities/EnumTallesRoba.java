/**
 * 
 */
package llibreries_i_utilities;

/**
 * @author Pol Ruiz
 *
 * 1r Daw
 * 03-04-2023
 */
public enum EnumTallesRoba {
	XS("US:30-32, EURO:34, MX:28-30"), 
	S("US:34-36, EURO:36, MX:32-34"), 
	M("US:48-40, EURO:38-40, MX:36-38"), 
	L("US:42-44, EURO:42-44, MX:40-42"), 
	XL("US:46-48, EURO:46-48, MX:44-46"), 
	XXL("US:50-52, EURO:50-52, MX:48-50");
	
	private String talla;
	
	/**
	 * Assigna el text de les talles a la variable <b>talla</b>
	 * @param talla --> Text que tenen les talles
	 */
	private EnumTallesRoba(String talla) {
		this.talla = talla;
	}
	
	/**
	 * 
	 * @return Retorna les característiques de cada talla
	 */
	public String getTalla() {
		return talla;
	}
}
