/**
 * 
 */
package llibreries_i_utilities;

/**
 * @author Pol Ruiz
 *
 * 1r Daw
 * 03-04-2023
 */
public class Main {
	public static void main(String[] args) {
		UtilRandom.init(true);
				
		System.out.println("\n----------UTIL RANDOM----------\n");
		
		System.out.println("Numero: " + UtilRandom.generarNumero(10, 20));
		System.out.println("Color: " + UtilRandom.generarColor());
		System.out.println("Boolean: " + UtilRandom.generarBoolean(100));
		System.out.println("Telefon: " + UtilRandom.generarTelefon());
		System.out.println("Data: " + UtilRandom.generarData());
		System.out.println("Persona: " + UtilRandom.generarUsuari());
		
		System.out.println("\n----------LAMBDA----------\n");
		
		UtilLambda lambda = () -> System.out.println("Hola, mundo!");
		lambda.run();
		
		System.out.println("\n----------ENUM----------\n");

		for (EnumTallesRoba talles : EnumTallesRoba.values()) {
			System.out.println(talles + " --> " + talles.getTalla());			
		}
		System.out.println("");
		for (EnumCardinalitat talles : EnumCardinalitat.values()) {
			System.out.println(talles);			
		}
	}

}
