/**
 * 
 */
package llibreries_i_utilities;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

/**
 * @author Pol Ruiz
 *
 * 1r Daw
 * 27-03-2023
 */
public class UtilConsole {


	private static Scanner sc;
	
	/**
	 * Inicia un Scanner, sense aquesta funció no és possible utilitzar l' 
	 * @param Scanner - true o false
	 */
	public static void init(boolean scanner) {
		if (scanner) {
			sc = new Scanner(System.in).useLocale(Locale.US);			
		}
	}
	
	/**
	 * Tanca l'Scanner
	 */
	public static void close() {
		sc.close();
	}
	
	/**
	 * Demana un String i valida que sigui correcte
	 * @return Retorna un <b>String</b>
	 */
	public static String demanarString() {
		String txt;
		
		do {
			
			txt = sc.nextLine();

		} while (txt.matches(""));
		
		return txt;
	}
	
	/**
	 * Demana un Integer i valida que tingui un format correcte [2] o [+2] o [-2]
	 * @return Retorna un <b>Integer</b>
	 */
	public static int demanarInt() {
		String text = "";
		int num = 0;
				
		do {
			if (sc.hasNextInt()) {
				text = sc.nextLine();
			}
			if (text.matches("[+|-]?[\\d]+")) {
				num = Integer.parseInt(text);
			}
			
		} while (!text.matches("[\\d]+") && num > Integer.MAX_VALUE);
		
		return num;
	}
	
	/**
	 * Demana una expresió booleana (true, si, 1) o (false, no, 0)
	 * @return Retorna un <b>Boolean</b>
	 */
	public static boolean demanarBoolean() {
		String text = "";
		boolean bol = false;
				
		do {
			text = sc.nextLine().toLowerCase();
			
			if (text.equals("true") || text.equals("si") || text.equals("1")) {
				bol = true;
				
			}else if (text.equals("false") || text.equals("no") || text.equals("0")) {
				bol = false;
			}
			
		} while (!sc.hasNextBoolean());
		
		return bol;
	}
	
	/**
	 * Demana un Double en <b>format 0.0</b>
	 * @return Retorna un <b>Double</b>
	 */
	public static double demanarDouble() {
		String text = "";
		double num = 0.0;
				
		do {
			if (sc.hasNextDouble()) {
				text = sc.nextLine();
			}
			if (text.matches("^\\d*\\.\\d+|\\d+\\.\\d*$")) {
				num = Double.parseDouble(text);
			}
			
		} while (!text.matches("^\\d*\\.\\d+|\\d+\\.\\d*$") && num > Double.MAX_VALUE);
		
		return num;
	}
	
	/**
	 * Demana un DNI en <b>format (12345678A) o (12345678a)</b>
	 * @return Retorna un <b>String</b> amb el DNI
	 */
	public static String demanarDNI() {
		String dni;
		do {
			
			dni =sc.nextLine();

		} while (!dni.matches("[\\d]{8}([a-z A-Z])"));
				
		return dni.toUpperCase();
	}
	
	/**
	 * Demana una data en <b>format dd/mm/yyyy</b>
	 * @return Retorna un <b>LocalDate</b> amb la data
	 */
	public static LocalDate demanarData() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String dataS = "";
		
		do {
			System.out.print("(dd/mm/yyyy): ");
			dataS = sc.next();
			
			if (!dataS.matches("(0?[1-9]|[1-2][\\d]|3[0-1]) / (0?[1-9]|1[0-2]) / ([\\d]{4})")) {
				
				System.out.print("Data en format incorrecte");
			}
		} while (!dataS.matches("(0?[1-9]|[12][\\d]|3[01]) / (0?[1-9]|1[0-2]) / ([\\d]{4})"));
		
		
		LocalDate data = LocalDate.parse(dataS, dtf);
		String fecha = data.format(dtf);
		
		return data;		
	}
	
	/**
	 * Demana un Email en <b>format x@x.x</b>
	 * @return Retorna un <b>String</b> amb la data
	 */
	public static String demanarEmail() {
		String email;
		do {
			
			email = demanarString();
			if (!email.matches("([a-z A-Z]*)(@)([a-z A-Z]*)(.)([a-z A-Z]*)")) {
				System.out.println("Email incorrecte");
			}

		} while (!email.matches("([a-z A-Z]*)(@)([a-z A-Z]*)(.)([a-z A-Z]*)"));
		return email;
	}
	
	/**
	 * Demana un codi postal amb <b>format 12345</b>
	 * @return Retorna un <b>String</b> amb el codi postal
	 */
	public static String demanarCP() {
		String cp;
		do {
			
			cp = sc.nextLine();

		} while (!cp.matches("[\\d]{5}"));
		
		return cp;
	}
	
	/**
	 * Demana un numero de telefon amb <b>format (+34 123456789) o (123456789)</b>
	 * @return Retorna un <b>String</b> amb el número de telefon
	 */
	public static String demanarTelefon() {
		String telf;
		sc.next();
		do {
			telf = sc.nextLine();

		} while (telf.matches("([+]?[\\d]{2}?)([\\d]{9})"));
		return telf;
	}
	
	/**
	 * Demana una ruta d'arxius en <b>format (C:x\x) o (.\x\x) o (..\x\x) o (\\x\x)</b> 
	 * @return
	 */
	public static String demanarRuta() {
		String text = "";
		String ruta = "";
		
		do {

			text = sc.nextLine();
			
			if (text.matches("([a-zA-Z]:)?(\\\\[^<>:\"/\\\\|?*\\n]*)+")) {
				ruta = text;
			}
			
		} while (!text.matches("([a-zA-Z]:)?(\\\\[^<>:\"/\\\\|?*\\n]*)+"));
		
		return ruta;
	}
	
	/**
	 * Demana que s'introdueixi <b>"Y" per un si</b> o <b>"N" per no</b>
	 * @return Retorna <b>true</b> o <b>false</b>
	 */
	public static boolean confirmar() {
		
		System.out.println("Confirmar (Y/N): ");
		String conf = sc.nextLine().toUpperCase();
		
		return !conf.equals("N");
	}

}
