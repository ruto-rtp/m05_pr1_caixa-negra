/**
 * 
 */
package llibreries_i_utilities;

import java.time.LocalDate;
import java.util.Random;

/**
 * @author Pol Ruiz
 *
 * 1r Daw
 * 03-04-2023
 */
public class UtilRandom {

	private static Random r;
	
	/**
	 * Inicia el Random, sense aquesta funció no és possible utilitzar el Random 
	 * @param random --> true o false
	 */
	public static void init(boolean random) {
		if (random) {
			r = new Random();			
		}
	}
		
	/**
	 * Genera un numero aleatori entre els nombres introduits per parametre.
	 * @param ini --> Indica el nombres amb el qual <b>començarà</b> el numero aleatori
	 * @param fini  --> Indica el nombres amb el qual <b>acabarà</b> el numero random
	 * @return Retorna un <b>Integer</b> entre els valors introduits [(15, 20) --> 17]
	 */
	public static int generarNumero(int ini, int fini) {
		return r.nextInt(ini, fini+1);
	}	
	
	/**
	 * Genera un color en format rgb
	 * @return Retorna un <b>String</b> amb un color en rgb [rgb(135, 25, 255)]
	 */
	public static String generarColor() {
		
		int a = r.nextInt(256);
		int b = r.nextInt(256);
		int c = r.nextInt(256);
		
		return "rgb(" + a + ", " + b + ", " + c + ")";
	}
	
	/**
	 * Genera un booleà "true" en funció d'una <b>probabilitat en percentatge (x%)</b> introduida per paràmetre
	 * @param prob --> Probabilitat amb la que sortirà "true"
	 * @return Retorna <b>true</b> o <b>false</b> [(0) --> false], [(100) --> true], [(50) --> 1/2 true o 1/2 false]
	 */
	public static boolean generarBoolean(int prob) {
		
		boolean rtn = true;
		
		if (r.nextInt(0, 101) > prob) {
			rtn = false;
		}
		
		return rtn;
	}
	
	/**
	 * Genera un numero de telefon espanyol (+34) començat per 6/7
	 * @return Retorna un numero de telefon amb <b>abreviatura del país</b> i els nombres <b>separats per "-" cada 3 dígits</b> [+34 000-000-000]
	 */
	public static String generarTelefon() {
		String telf = "+34 ";
		
		telf += "" + r.nextInt(6, 8); //6 o 7
		
		for (int i = 0; i < 2; i++) {
			telf += r.nextInt(0, 10);
		}
		
		for (int i = 0; i < 2; i++) {
			telf += "-";
			for (int j = 0; j < 3; j++) {
				telf += r.nextInt(0, 10);
			}
			
		}
		
		return telf;
	}
	
	/**
	 * Genera una data aleatoria des de l'any 0 <b>fins l'actual</b>
	 * @return Retorna una data en <b>format dd/mm/yyyy</b> [1/7/420]
	 */
	public static String generarData() {
		int any = r.nextInt(0, LocalDate.now().getYear());
		int mes = r.nextInt(1, 13);
		int dia = 0;
		
		if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
			dia = r.nextInt(1, 32);
		}
		if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
			dia = r.nextInt(1, 31);
		}
		if (mes ==2) {
			if ((any%4 == 0 && any%100 == 0) || (any%400 == 0)) {
				dia = r.nextInt(1, 30);
			}else {
				dia = r.nextInt(1, 29);
			}
		}
		
		return dia + "/" + mes + "/" + any;
	}
	
	/**
	 * Genera el nom d'una persona tant del genere masculí com del femení, amb els seus respectius cognoms
	 * @return Retorna el nom d'una persona aleatoria en <b>format Nom Cognom Cognom</b> [Richard David Miller]
	 */
	public static String generarUsuari() {
		String[] noms = {"James", "John", "Robert", "Michael", "William", "David", "Richard", "Joseph", "Thomas", "Charles", "Christopher", "Daniel", "Matthew", "Anthony", "Mark", "Mary", "Patricia", "Jennifer", "Linda", "Elizabeth", "Barbara", "Susan", "Jessica", "Sarah", "Karen", "Nancy", "Lisa", "Betty", "Margaret", "Sandra", "Ashley", "Emily", "Samantha", "Olivia", "Emma", "Isabella", "Ava", "Mia", "Sophia", "Charlotte", "Amelia", "Harper", "Evelyn", "Abigail", "Chloe", "Victoria", "Grace", "Zoe", "Scarlett", "Madison", "Eleanor", "Penelope", "Hazel", "Violet", "Lucy", "Stella", "Audrey", "Claire", "Caroline", "Leah", "Benjamin", "Ethan", "Noah", "William", "James", "Jacob", "Alexander", "Michael", "Daniel", "Matthew", "Aiden", "Henry", "Joseph", "Samuel", "Joshua", "David", "Gabriel", "Christopher", "Noah", "Liam", "Mason", "Elijah", "Logan", "Caleb", "Andrew", "Samuel", "Luke", "Grayson", "Jackson", "Carter", "Dylan", "Oliver", "Wyatt", "Owen", "Hunter", "Christian", "Levi", "Xavier", "Sebastian", "Brandon", "Adam", "Julian", "Aaron", "Caleb", "Gavin", "Josiah", "Brayden", "Evan", "Isaiah", "Tyler", "Alex", "Isaac", "Evan", "Jaxon", "Liam", "Luke", "Jason", "Brian", "Alex", "Cameron", "Kevin", "Tristan", "Austin", "Isaac", "Justin", "Bryce", "Jackson", "Nathan", "Dylan", "Dominic", "Adam", "Jonathan", "Antonio", "Jaden", "Anthony", "Richard", "Nolan", "Ian", "Xavier", "Cole", "Tyler", "Adrian", "Owen", "Camden", "Jonathan", "Chase", "Colton", "Sean", "Vincent", "Peter", "Juan", "Christopher", "Leo", "Joseph", "Edward", "Dylan", "David", "Jeremy", "Luis", "Ryan", "Leo", "Carlos", "Jose", "Jason", "Gabriel", "Juan", "Colin", "Jordan", "Joel", "Tyler", "Antonio", "Ethan", "Kyle", "Brian", "Logan", "Luke", "Isaiah"};
		String[] cognoms = {"Smith", "Johnson", "Brown", "Taylor", "Miller", "Davis", "Wilson", "Anderson", "Thomas", "Jackson", "White", "Harris", "Martin", "Thompson", "Moore", "Allen", "Young", "King", "Wright", "Scott", "Green", "Baker", "Adams", "Nelson", "Carter", "Mitchell", "Perez", "Roberts", "Turner", "Phillips", "Campbell", "Parker", "Evans", "Edwards", "Collins", "Stewart", "Sanchez", "Morris", "Rogers", "Reed", "Cook", "Morgan", "Bell", "Murphy", "Bailey", "Rivera", "Cooper", "Richardson", "Cox", "Howard", "Ward", "Torres", "Peterson", "Gray", "Ramirez", "James", "Watson", "Brooks", "Kelly", "Sanders", "Price", "Bennett", "Wood", "Barnes", "Ross", "Henderson", "Coleman", "Jenkins", "Perry", "Powell", "Long", "Patterson", "Hughes", "Flores", "Washington", "Butler", "Simmons", "Foster", "Gonzales", "Bryant", "Alexander", "Russell", "Griffin", "Diaz", "Hayes", "Myers", "Ford", "Hamilton", "Graham", "Sullivan", "Wallace", "Woods", "Cole", "West", "Jordan", "Owens", "Reynolds", "Fisher", "Ellis", "Harrison", "Gibson", "Mcdonald", "Cruz", "Marshall", "Ortiz", "Gomez", "Murray", "Freeman", "Wells", "Webb", "Simpson", "Stevens", "Tucker", "Porter", "Hunter"};

		return noms[r.nextInt(101)] + " " + cognoms[r.nextInt(101)] + " " + cognoms[r.nextInt(101)];
	}
}
